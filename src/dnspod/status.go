package dnspod

// Status status
type Status struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}
