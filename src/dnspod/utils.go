package dnspod

import (
	"fmt"
	"strconv"

	"github.com/antonholmquist/jason"
)

func getID(obj *jason.Object) int {
	id, err := obj.GetInt64("id")
	if err != nil {
		str, err := obj.GetString("id")
		assert(err)
		id, err = strconv.ParseInt(str, 10, 64)
		assert(err)
	}
	return int(id)
}

func checkResponseCode(obj *jason.Object, api string, wantPanic bool) error {
	code, err := obj.GetString("status", "code")
	assert(err)
	if code != "1" {
		msg, err := obj.GetString("status", "message")
		assert(err)
		err = fmt.Errorf("%s call failed, code=%s, msg=%s", api, code, msg)
		if wantPanic {
			panic(err)
		}
		return err
	}
	return nil
}
