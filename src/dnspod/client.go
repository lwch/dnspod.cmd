package dnspod

// Client client
type Client struct {
	id    string
	token string
}

// NewClient create client
func NewClient(id, token string) *Client {
	return &Client{id, token}
}
