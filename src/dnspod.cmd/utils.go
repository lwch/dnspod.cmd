package main

import (
	"dnspod"
	"strings"
)

func lookupRecord(domain, record string) ([]dnspod.Record, bool) {
	_, records, err := cli.RecordList(domain, record)
	if err != nil {
		if strings.Index(err.Error(), "msg=No records") != -1 {
			return nil, false
		}
	}

	var ret []dnspod.Record
	for _, r := range records {
		if r.Name == record {
			ret = append(ret, r)
		}
	}
	return ret, len(ret) > 0
}
