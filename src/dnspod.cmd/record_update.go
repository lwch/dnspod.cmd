package main

import (
	"flag"
	"fmt"
	"os"
)

func recordUpdate() {
	fs := flag.NewFlagSet("record-update", flag.ExitOnError)
	fs.Usage = func() {
		fmt.Fprintln(os.Stderr, `Usage: dnspod.cmd record-update OPTIONS VALUE

VALUE is the ip address of type=A, otherwise...

OPTIONS:`)
		fs.PrintDefaults()
	}
	parseCommonArgs(fs)
	domain := fs.String("domain", "", "domain name")
	record := fs.String("record", "", "record name")
	t := fs.String("type", "A", "record type")
	parseArgs(fs)

	if len(confDir) == 0 {
		fmt.Println("Missing conf param")
		os.Exit(1)
	}
	if len(*domain) == 0 {
		fmt.Println("Missing domain param")
		os.Exit(1)
	}
	if len(*record) == 0 {
		fmt.Println("Missing record param")
		os.Exit(1)
	}
	if len(*t) == 0 {
		fmt.Println("Missing type param")
		os.Exit(1)
	}

	if fs.NArg() == 0 {
		fmt.Println("Missing VALUE")
		os.Exit(1)
	}

	LoadConf(confDir)

	targets, ok := lookupRecord(*domain, *record)
	if !ok {
		fmt.Printf("Record `%s.%s` not found\n", *domain, *record)
		os.Exit(1)
	}

	if len(targets) == 1 {
		if !force && !confirm(fmt.Sprintf("Change `%s.%s`: `%s,%s` => `%s,%s`?",
			*record, *domain,
			targets[0].Type, targets[0].Value,
			*t, fs.Arg(0))) {
			os.Exit(0)
		}
	} else {
		fmt.Printf("Remove `%s.%s` have more than 1 records, not supported yet\n", *domain, *record)
		os.Exit(1)
	}

	// update
	_, err := cli.UpdateRecord(targets[0].ID, *domain, *record, *t, fs.Arg(0))
	assert(err)

	fmt.Printf("Update `%s.%s,%s,%s` success\n", *record, *domain, *t, fs.Arg(0))
}
