package main

import (
	"flag"
	"fmt"
	"html/template"
	"os"
	"text/tabwriter"
)

func domainList() {
	fs := flag.NewFlagSet("domain-list", flag.ExitOnError)
	fs.Usage = func() {
		fmt.Fprintln(os.Stderr, `Usage: dnspod.cmd domain-list OPTIONS

OPTIONS:`)
		fs.PrintDefaults()
	}
	parseCommonArgs(fs)
	keyword := fs.String("keyword", "", "query keyword")
	parseArgs(fs)

	defaultFormats := "{{.Name}}\t{{.Status}}\t{{.ExtStatus}}\t{{.Records}}"
	if allowFormats {
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 8, ' ', 0)
		fmt.Fprintln(w, "Allow:\t"+defaultFormats+"\t{{.ID}}\t")
		fmt.Fprintln(w, "Default:\t"+defaultFormats+"\t")
		w.Flush()
		os.Exit(0)
	}

	if len(confDir) == 0 {
		fmt.Println("Missing conf param")
		os.Exit(1)
	}

	LoadConf(confDir)

	if len(format) > 0 {
		defaultFormats = format
	}
	tpl, err := template.New("row").Parse(defaultFormats)
	assert(err)

	domains, err := cli.DomainList(*keyword)
	assert(err)
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 8, ' ', 0)
	defer w.Flush()
	if !noHeader {
		assert(tpl.Execute(w, map[string]string{
			"ID":        "id",
			"Name":      "name",
			"Status":    "status",
			"ExtStatus": "ext_status",
			"Records":   "records",
		}))
		fmt.Fprintln(w)
	}
	for _, domain := range domains {
		assert(tpl.Execute(w, domain))
		fmt.Fprintln(w)
	}
}
