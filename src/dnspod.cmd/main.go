package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

var confDir string
var format string
var noHeader bool
var force bool
var allowFormats bool

func parseCommonArgs(fs *flag.FlagSet) {
	fs.StringVar(&confDir, "conf", "", "configure file path")
	fs.StringVar(&format, "format", "", "output format, template supported")
	fs.BoolVar(&noHeader, "no-header", false, "do not show header")
	fs.BoolVar(&force, "f", false, "force run")
	fs.BoolVar(&allowFormats, "allow-formats", false, "show allow formats")
}

func parseArgs(fs *flag.FlagSet) {
	fs.Parse(os.Args[2:])
}

func confirm(str string) bool {
	fmt.Print(str + " ")
	s := bufio.NewScanner(os.Stdin)
	s.Scan()
	str = strings.ToLower(s.Text())
	return str == "y" || str == "yes"
}

func showUsage() {
	fmt.Fprintln(os.Stderr, `
Usage: dnspod.cmd COMMAND

Common Options:
  -conf        configure file path
  -format      output format, template supported
  -f           force run
  -no-header   do not show header

Domain Commands:
  domain-list     list domains in this account

Record Commands:
  record-list     list records
  record-add      add record
  record-update   update record
  record-remove   remove record
  record-batch    import sub domain list or remove

Other Commands:
  version         show version info

Run 'dnspod.cmd COMMAND --help' for more information on a command`)
}

func main() {
	if len(os.Args) <= 1 {
		showUsage()
		os.Exit(1)
	}

	switch os.Args[1] {
	// domain
	case "domain-list":
		domainList()
	// record
	case "record-list":
		recordList()
	case "record-add":
		recordAdd()
	case "record-update":
		recordUpdate()
	case "record-remove":
		recordRemove()
	case "record-batch":
		recordBatch()
	// other
	case "version":
		fmt.Println(verinfo())
		os.Exit(0)
	case "-help", "-h":
		showUsage()
	default:
		fmt.Printf("Undefined command %s\n", os.Args[1])
		showUsage()
		os.Exit(1)
	}
}
