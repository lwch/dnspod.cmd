package main

import (
	"flag"
	"fmt"
	"os"
)

func recordAdd() {
	fs := flag.NewFlagSet("record-add", flag.ExitOnError)
	fs.Usage = func() {
		fmt.Fprintln(os.Stderr, `Usage: dnspod.cmd record-add OPTIONS VALUE

VALUE is the ip address of type=A, otherwise...

OPTIONS:`)
		fs.PrintDefaults()
	}
	parseCommonArgs(fs)
	domain := fs.String("domain", "", "domain name")
	record := fs.String("record", "", "record name")
	t := fs.String("type", "A", "record type")
	parseArgs(fs)

	if len(confDir) == 0 {
		fmt.Println("Missing conf param")
		os.Exit(1)
	}
	if len(*domain) == 0 {
		fmt.Println("Missing domain param")
		os.Exit(1)
	}
	if len(*record) == 0 {
		fmt.Println("Missing record param")
		os.Exit(1)
	}
	if len(*t) == 0 {
		fmt.Println("Missing type param")
		os.Exit(1)
	}

	if fs.NArg() == 0 {
		fmt.Println("Missing VALUE")
		os.Exit(1)
	}

	LoadConf(confDir)

	_, err := cli.AddRecord(*domain, *record, *t, fs.Arg(0))
	assert(err)

	fmt.Printf("Add `%s.%s,%s,%s` success\n", *record, *domain, *t, fs.Arg(0))
}
