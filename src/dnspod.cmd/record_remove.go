package main

import (
	"flag"
	"fmt"
	"os"
)

func recordRemove() {
	fs := flag.NewFlagSet("record-update", flag.ExitOnError)
	fs.Usage = func() {
		fmt.Fprintln(os.Stderr, `Usage: dnspod.cmd record-remove OPTIONS

OPTIONS:`)
		fs.PrintDefaults()
	}
	parseCommonArgs(fs)
	domain := fs.String("domain", "", "domain name")
	record := fs.String("record", "", "record name")
	parseArgs(fs)

	if len(confDir) == 0 {
		fmt.Println("Missing conf param")
		os.Exit(1)
	}
	if len(*domain) == 0 {
		fmt.Println("Missing domain param")
		os.Exit(1)
	}
	if len(*record) == 0 {
		fmt.Println("Missing record param")
		os.Exit(1)
	}

	LoadConf(confDir)

	targets, ok := lookupRecord(*domain, *record)
	if !ok {
		fmt.Printf("Record `%s.%s` not found\n", *domain, *record)
		os.Exit(1)
	}

	if len(targets) == 1 {
		if !force && !confirm(fmt.Sprintf("Remove `%s.%s,%s,%s`?",
			*record, *domain, targets[0].Type, targets[0].Value)) {
			os.Exit(0)
		}
	} else {
		fmt.Printf("Remove `%s.%s` have more than 1 records, not supported yet\n", *domain, *record)
		os.Exit(1)
	}

	// update
	err := cli.RemoveRecord(targets[0].ID, *domain)
	assert(err)

	fmt.Printf("Remove `%s.%s,%s,%s` success\n", *record, *domain, targets[0].Type, targets[0].Value)
}
